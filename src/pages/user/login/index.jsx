import { AlipayCircleOutlined, TaobaoCircleOutlined, WeiboCircleOutlined } from '@ant-design/icons';
import { Alert, Checkbox } from 'antd';
import React, { useState } from 'react';
import { Link } from 'umi';
import { connect } from 'dva';
import styles from './style.less';
import LoginFrom from './components/Login';
const { Tab, UserName, Password, Mobile, Captcha, Submit } = LoginFrom;

const LoginMessage = ({ content }) => (
  <Alert
    style={{
      marginBottom: 24,
    }}
    message={content}
    type="error"
    showIcon
  />
);

const Login = props => {
  const { userLogin = {}, submitting } = props;
  const { status, type: loginType } = userLogin;
  const [autoLogin, setAutoLogin] = useState(true);
  const [type, setType] = useState('account');

  const handleSubmit = values => {
    const { dispatch } = props;
    dispatch({
      type: 'login/login',
      payload: { ...values, type },
    });
  };

  return (
    <div className={styles.main}>
      <LoginFrom activeKey={type} onTabChange={setType} onSubmit={handleSubmit}>
        <Tab key="account" tab="Account Password Login">
          {status === 'error' && loginType === 'account' && !submitting && (
            <LoginMessage content="账户或密码错误（admin/ant.design）" />
          )}

          <UserName
            name="userName"
            placeholder="Enter your username"
            rules={[
              {
                required: true,
                message: 'Please enter your username!',
              },
            ]}
          />
          <Password
            name="password"
            placeholder="Enter your password"
            rules={[
              {
                required: true,
                message: 'Please enter your password！',
              },
            ]}
          />
        </Tab>
        <Tab key="mobile" tab="Mobile Number Login">
          {status === 'error' && loginType === 'mobile' && !submitting && (
            <LoginMessage content="Verification code error" />
          )}
          <Mobile
            name="mobile"
            placeholder="Phone Number"
            rules={[
              {
                required: true,
                message: 'Please enter your phone number！',
              },
              {
                pattern: /^1\d{10}$/,
                message: 'Malformed phone number！',
              },
            ]}
          />
          <Captcha
            name="captcha"
            placeholder="Verification code"
            countDown={120}
            getCaptchaButtonText=""
            getCaptchaSecondText="second"
            rules={[
              {
                required: true,
                message: 'Please enter your verification code！',
              },
            ]}
          />
        </Tab>
        <div>
          <Checkbox checked={autoLogin} onChange={e => setAutoLogin(e.target.checked)}>
            Keep me logged in.
          </Checkbox>
          <a
            style={{
              float: 'right',
            }}
          >
            Forgot Password
          </a>
        </div>
        <Submit loading={submitting}>Login</Submit>
        <div className={styles.other}>
          Sign in with
          <AlipayCircleOutlined className={styles.icon} />
          <TaobaoCircleOutlined className={styles.icon} />
          <WeiboCircleOutlined className={styles.icon} />
          <Link className={styles.register} to="/user/register">
            Sign Up
          </Link>
        </div>
      </LoginFrom>
    </div>
  );
};

export default connect(({ login, loading }) => ({
  userLogin: login,
  submitting: loading.effects['login/login'],
}))(Login);
